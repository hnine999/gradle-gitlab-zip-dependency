plugins {
    id("java")
}

val gitLabPersonalAccessToken: String by project

repositories {
    // mavenLocal()
    // mavenCentral()
    maven {
        url = uri("https://gitlab.com/api/v4/projects/37411709/packages/maven")
        name = "DM_Repo_Private"

        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = gitLabPersonalAccessToken
        }
        authentication{
            create<HttpHeaderAuthentication>("header")
        }
    }
}

dependencies {
    // https://mvnrepository.com/artifact/com.google.code.gson/gson
    implementation("com.google.code.gson:gson:2.9.0")

    implementation(group = "com.google.code.foo", name = "foo", version = "2.9.0", ext = "zip")
}

